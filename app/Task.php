<?php

namespace App;

use App\Http\Controllers\TimeParser;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    public function stopAllTasks()
    {
        $active_tasks = $this::where('active', 1)->get();
        foreach($active_tasks as $task){
            $this->stopTask($task);
        }
    }


    public function getAllTasksToday($fields = null)
    {
        $today = (new TimeParser)->todayDatetime();
        $tasks = $this::where('created_at','>', $today);

        if (!empty($fields))
                $tasks->selectRaw(implode(',', $fields));

        return $tasks
            ->orderBy('created_at', 'desc')
            ->get();
    }


    public function existentTaskToday($task_name)
    {
        $task_name = strtolower($task_name);
        $today = (new TimeParser)->todayDatetime();
        return $this::where([['task', $task_name],['created_at','>', $today]])->first();
    }


    public function createTask($task_name)
    {
        $this->stopAllTasks();
        $task_name = strtolower($task_name);
        $new_task = new $this;
        $new_task->task = $task_name;
        return $new_task->save();
    }


    public function startTask(Task $task)
    {
        $this->stopAllTasks();
        $task->active = 1;
        return $task->save();
    }


    public function stopTask(Task $task)
    {
        $increment_in_seconds = (new TimeParser)->fromLastActivityToNowInSeconds($task->state_changed);
        $task->active = 0;
        $task->seconds += $increment_in_seconds;
        return $task->save();
    }


    public function totalDailyTaskTime ()
    {
        $today = (new TimeParser)->todayDatetime();

        return $this
            ->where('created_at', '>', $today)
            ->select('seconds')
            ->sum('seconds');
    }


    public function updateTask($task_id, $task_name)
    {
        $task = $this->findOrFail($task_id);
        $task->task = $task_name;
        return $task->save();
    }


    public function destroyTask($task_name)
    {
        return $this->where('task', $task_name)->delete();
    }

}
