<?php

namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;

class GetAllTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-all-tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It returns a list of all tasks';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $headers = ['Name', 'Status', 'Time in seconds', 'Last modification'];
            $fields = ['task', 'active', 'seconds', 'state_changed'];
            $tasks = new Task;
            $tasks_today = $tasks->getAllTasksToday($fields)->toArray();

            if(!$tasks){
                $this->line('There are no tasks to display');
            } else {
                $this->table($headers, $tasks_today);
            }
        } catch (\Exception $e) {
            $this->error('Something went wrong...');
        }
    }
}
