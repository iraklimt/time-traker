<?php

namespace App\Console\Commands;

use App\TaskAdapter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class PerformTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'perform-task {action} {task}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Starts or stops a task. Two parameters are required: action (start/end) and task';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        $task_name = $this->argument('task');

        $validator = Validator::make([
            'action' => $action,
            'task' => $task_name,
        ], [
            'action' => 'required|in:start,end',
            'task' => 'required|max:100|min:3',
        ]);

        if($validator->fails()){
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }

        try{

            $taskAdapter = new TaskAdapter;
            $taskAdapter->createOrUpdate($task_name, $action);
            $this->info($taskAdapter->responseMessage);

        } catch (\Exception $e) {
            $this->error('Something went wrong...');
        }
    }
}
