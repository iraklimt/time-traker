<?php

namespace App;

class TaskAdapter
{

    public $responseMessage = 'task updated';

    public function createOrUpdate($task_name, $action = null)
    {
        $task = new Task;
        $task_name = strtolower($task_name);
        $task = $task->existentTaskToday($task_name);

        if ($task) {

            if ($action === 'start' && $task->active === 0) return $task->startTask($task);
            if ($action === 'end' && $task->active === 1) return $task->stopTask($task);
            if ($action === null && $task->active === 0) return $task->startTask($task);
            if ($action === null && $task->active === 1) return $task->stopTask($task);

        } else {

            if ($action !== 'end') {

                $task = new Task;
                $task->createTask($task_name);
                $this->responseMessage = 'new task created';
            }
        }
    }


}
