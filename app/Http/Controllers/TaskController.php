<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskAdapter;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = new Task;
        $tasks = $task->getAllTasksToday();
        $daily_seconds = $task->totalDailyTaskTime();
        return view('tasks', compact('tasks','daily_seconds'));

    }

    private function validateInputs($request)
    {
        $request->validate([
            'task.id' => 'integer',
            'task.seconds' => 'integer',
            'task.task' => 'required|string|min:3|max:100',
        ]);
        return $request;
    }


    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // If not valid returns 422 json error response
        $this->validateInputs($request);
        $task_name = $request->input('task.task');

        $taskAdapter = new TaskAdapter;
        $taskAdapter->createOrUpdate($task_name);
        $message = $taskAdapter->responseMessage;

        return response()->json([
            'msg' => $message,
        ],200);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validateInputs($request);
        $task_id = $request->input('task.id');
        $task_name = $request->input('task.task');

        $task = new Task;
        $task->updateTask($task_id, $task_name);

        return response()->json([
            'msg' => 'The task was updated',
        ],200);
    }


    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validateInputs($request);
        $task_name = $request->input('task.task');
        $task = new Task;
        $task->destroyTask($task_name);

        return response()->json([
            'msg' => 'The task was deleted',
        ], 200);
    }
}
