<?php

namespace App\Http\Controllers;


class TimeParser
{

    private $time_zone;
    private $now;


    function __construct()
    {
        $this->time_zone = new \DateTimeZone('UTC');
        $this->now = new \DateTime('now', $this->time_zone);
    }


    public function todayDatetime()
    {
        return $this->now->format('Y-m-d 00:00:00');
    }


    public function fromLastActivityToNowInSeconds($last_activity)
    {
        $last_activity = new \DateTime($last_activity, $this->time_zone);
        return (int)(($this->now->format('U') - $last_activity->format('U')));
    }

}