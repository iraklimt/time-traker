# Project overview
The main goal of this project is to create a little application to show general skills on web app development.

The web application is a time tracker where a user can create tasks, as well as update their content (clicking over the task description), their status, and of course, delete them.


## Architecture
This project is based on PHP Laravel (backend) + VueJs (frontend), and is deployed on Docker ecosystem (Nginx + PHP + MySQL), concretely using [Docker Compose](https://docs.docker.com/compose/), so the recommended way to run it is executing the docker-compose.yml file.

All the necessary files and volumes (folder bindings) to run and deploy this application are included in this folder.

## Procedure
The steps that I followed for creating this application were, first, test and create the docker ecosystem writing the docker-compose and dockerfile files. Then I created a Laravel boilerplate and installed all the necessary packages, including composer and node (meanly for vue js).

For avoid a docker container oversizing and keep them as lightweight as possible, I run Composer and NPM from outer containers, pointing to my app folder with volume bindings.

The instructions that I followed are here:

#### composer
```
  docker run --rm \
  -v $(pwd):/app \
  composer install
```

#### node & npm
```
  docker run --rm \
  --volume "$(pwd):/app" \
  --workdir "/app" \
  --user "$(id -u):$(id -g)" \
  node npm install && npm run dev 
```

Once the working environment was ready I started working, first in a view skeleton, where I spend the most part of the time and I have to recognize that it has some parts that could be done better, but for a showcase it works. All the code is made by Vue using single file components.

On the other hand, regarding the backend, the main task is performed by TaskController and the Task model. The is just the necessary to accomplish the requirements. The Console commands are handled by GetAllTasks and PerformTask classes.

Regarding the database, I used MySQL and a single table named taks (:P).
To design the table as it was up to me I decided to include the next columns: Id, task (task description), seconds(total lapse time of the task), active(current status), and state_changed (a datetime to control the last update). 
Each time an user updates his task, I increment the difference between the last update and the current timestamp.

For run the console commands, you can write either:

```
  docker-compose exec app php artisan perform-task (action) (task description)
  docker-compose exec app php artisan get-all-tasks
```

The first part of the commands (docker-compose exec app) is just to access the docker container where is allocated the app.

To migrate the database out of the box, you can use:
```
  docker-compose exec app php artisan migrate
```

#### Access to service terminals
- `docker-compose exec app /bin/bash`
- `docker-compose exec db /bin/bash`
- `docker-compose exec webserver /bin/sh`


   

