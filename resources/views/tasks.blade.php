<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TimeTracker</title>
        <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    </head>

    <body>
        <div id="app" class="app-container">
            <main-component raw-tasks="{{$tasks}}" raw-daily-seconds="{{$daily_seconds}}">
                <!-- Suxe: Passing App name as 'slot' -->
                {{config('app.name')}}
            </main-component>
        </div>

        <footer id="footer">
            footer
        </footer>

        <!-- JS file -->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
